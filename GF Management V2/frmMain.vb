﻿Public Class frmMain

    Private Sub CloseAllMdiChild()
        For Each frm As Form In Me.MdiChildren
            frm.Close()
            frm.Dispose()
        Next
    End Sub

    Private Sub รายวนToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles รายวนToolStripMenuItem.Click
        Dim main As New frmEmpday
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub รายเดอนToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles รายเดอนToolStripMenuItem.Click
        Dim main As New frmEmpmonth
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub สรางชอแผนกและกลมงานToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles สรางชอแผนกและกลมงานToolStripMenuItem.Click
        Dim main As New frmDepartment
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub กำหนดตำแหนงToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles กำหนดตำแหนงToolStripMenuItem.Click
        Dim main As New frmPosition
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub กำหนดเวลาเขาออกงานกะToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles กำหนดเวลาเขาออกงานกะToolStripMenuItem.Click
        Dim main As New frmSettime
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub กำหนดวนหยดประจำปToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles กำหนดวนหยดประจำปToolStripMenuItem.Click
        Dim main As New frmHoliday
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub กำหนดคาลวงเวลาToolStripMenuItem_Click(sender As Object, e As EventArgs) 
        Dim main As New frmOttype
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub กำหนดคาประกนสงคมToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles กำหนดคาประกนสงคมToolStripMenuItem.Click
        Dim main As New frmSocialsecurity
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub จดกลมการทำงานToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim main As New frmGrouptime
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub นำเขาขอมลจากFlashdriveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles นำเขาขอมลจากFlashdriveToolStripMenuItem.Click
        Dim main As New frmInsertflashdrive
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub คนหาขอมลตางๆToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim main As New frmAllsearch
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub ขอทำลวงเวลารายบคคลToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ขอทำลวงเวลารายบคคลToolStripMenuItem.Click
        Dim main As New frmOt_sasom
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem.Click
        Dim main As New frmGroupot_sasom
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub บนทกขอมลขอออกกอนToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles บนทกขอมลขอออกกอนToolStripMenuItem.Click
        Dim main As New frmPersonoutbefore
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub บนทกขอออกงานกอนแบบกลมToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles บนทกขอออกงานกอนแบบกลมToolStripMenuItem.Click
        Dim main As New frmGroupoutbefore
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub บนทกขอเขาทำงานชดเชยเวลาToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim main As New frmPersoncompensated
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub บนทกคาเสยหายToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles บนทกคาเสยหายToolStripMenuItem.Click
        Dim main As New frmOthervalue
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub การคนหาขอมลตงๆToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles การคนหาขอมลตงๆToolStripMenuItem.Click
        Dim main As New frmAllsearch
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub บนทกการลาToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles บนทกการลาToolStripMenuItem.Click
        Dim main As New frmLeave
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub รายงานการเขาหองนำToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles รายงานการเขาหองนำToolStripMenuItem.Click
        Dim main As New frmToilet
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub สรางกลมงานToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles สรางกลมงานToolStripMenuItem.Click
        Dim main As New frmGroup
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub บนทกเปลยนOTเปนสะสมToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles บนทกเปลยนOTเปนสะสมToolStripMenuItem.Click
        Dim main As New frmChangOt
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub กำหนดเงนเดอนToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles กำหนดเงนเดอนToolStripMenuItem.Click
        Dim main As New frmSalaryhis
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub กำหนดเงนรายวนToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles กำหนดเงนรายวนToolStripMenuItem.Click
        Dim main As New frmWageshis
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub กำหนดหวหนาคมงานToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim main As New frmLeader
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub ยายกลมToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim main As New frmRemoveGroup
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub บนทกขอเขางานสายToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles บนทกขอเขางานสายToolStripMenuItem.Click
        Dim main As New frmLate
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub จดกลมงานToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles จดกลมงานToolStripMenuItem.Click
        Dim main As New frmGrouptime
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub ยายสมาชกในกลมงานToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ยายสมาชกในกลมงานToolStripMenuItem.Click
        Dim main As New frmRemoveGroup
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub

    Private Sub แตงตงหวหนากลมToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles แตงตงหวหนากลมToolStripMenuItem.Click
        Dim main As New frmLeader
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub




    Private Sub บนทกขอทำชดเชยรายบคคลToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles บนทกขอทำชดเชยรายบคคลToolStripMenuItem.Click
        Dim main As New frmPersoncompensated
        CloseAllMdiChild()
        main.MdiParent = Me
        main.Show()
    End Sub
End Class
