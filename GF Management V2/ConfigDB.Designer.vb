﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfigDB
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblServername = New System.Windows.Forms.Label()
        Me.txtServername = New System.Windows.Forms.TextBox()
        Me.txtDatabasename = New System.Windows.Forms.TextBox()
        Me.lblDatabasename = New System.Windows.Forms.Label()
        Me.txtUsername = New System.Windows.Forms.TextBox()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.btSave = New System.Windows.Forms.Button()
        Me.btCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblServername
        '
        Me.lblServername.AutoSize = True
        Me.lblServername.Location = New System.Drawing.Point(65, 50)
        Me.lblServername.Name = "lblServername"
        Me.lblServername.Size = New System.Drawing.Size(87, 29)
        Me.lblServername.TabIndex = 0
        Me.lblServername.Text = "Servername :"
        '
        'txtServername
        '
        Me.txtServername.Location = New System.Drawing.Point(158, 47)
        Me.txtServername.Name = "txtServername"
        Me.txtServername.Size = New System.Drawing.Size(202, 36)
        Me.txtServername.TabIndex = 1
        '
        'txtDatabasename
        '
        Me.txtDatabasename.Location = New System.Drawing.Point(158, 89)
        Me.txtDatabasename.Name = "txtDatabasename"
        Me.txtDatabasename.Size = New System.Drawing.Size(202, 36)
        Me.txtDatabasename.TabIndex = 3
        '
        'lblDatabasename
        '
        Me.lblDatabasename.AutoSize = True
        Me.lblDatabasename.Location = New System.Drawing.Point(52, 92)
        Me.lblDatabasename.Name = "lblDatabasename"
        Me.lblDatabasename.Size = New System.Drawing.Size(100, 29)
        Me.lblDatabasename.TabIndex = 2
        Me.lblDatabasename.Text = "Databasename :"
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(158, 131)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(202, 36)
        Me.txtUsername.TabIndex = 5
        '
        'lblUsername
        '
        Me.lblUsername.AutoSize = True
        Me.lblUsername.Location = New System.Drawing.Point(76, 134)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(76, 29)
        Me.lblUsername.TabIndex = 4
        Me.lblUsername.Text = "Username :"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(158, 173)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(202, 36)
        Me.txtPassword.TabIndex = 7
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Location = New System.Drawing.Point(76, 176)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(73, 29)
        Me.lblPassword.TabIndex = 6
        Me.lblPassword.Text = "Password :"
        '
        'btSave
        '
        Me.btSave.Location = New System.Drawing.Point(158, 215)
        Me.btSave.Name = "btSave"
        Me.btSave.Size = New System.Drawing.Size(83, 47)
        Me.btSave.TabIndex = 8
        Me.btSave.Text = "บันทึก"
        Me.btSave.UseVisualStyleBackColor = True
        '
        'btCancel
        '
        Me.btCancel.Location = New System.Drawing.Point(277, 215)
        Me.btCancel.Name = "btCancel"
        Me.btCancel.Size = New System.Drawing.Size(83, 47)
        Me.btCancel.TabIndex = 9
        Me.btCancel.Text = "ยกเลิก"
        Me.btCancel.UseVisualStyleBackColor = True
        '
        'ConfigDB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 29.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(498, 291)
        Me.Controls.Add(Me.btCancel)
        Me.Controls.Add(Me.btSave)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.txtUsername)
        Me.Controls.Add(Me.lblUsername)
        Me.Controls.Add(Me.txtDatabasename)
        Me.Controls.Add(Me.lblDatabasename)
        Me.Controls.Add(Me.txtServername)
        Me.Controls.Add(Me.lblServername)
        Me.Font = New System.Drawing.Font("Angsana New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 7, 4, 7)
        Me.Name = "ConfigDB"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "การตั้งค่าฐานข้อมูล"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblServername As Label
    Friend WithEvents txtServername As TextBox
    Friend WithEvents txtDatabasename As TextBox
    Friend WithEvents lblDatabasename As Label
    Friend WithEvents txtUsername As TextBox
    Friend WithEvents lblUsername As Label
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents lblPassword As Label
    Friend WithEvents btSave As Button
    Friend WithEvents btCancel As Button
End Class
