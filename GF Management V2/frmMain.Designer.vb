﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.บนทกประวตToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายวนToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายเดอนToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดเงนเดอนToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดเงนรายวนToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.การนำเขาขอมลToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.นำเขาขอมลจากFlashdriveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอมลตางๆToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ขอทำลวงเวลารายบคคลToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอมลขอออกกอนToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอเขางานสายToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกการลาToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกคาเสยหายToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายงานการเขาหองนำToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.การคนหาขอมลตงๆToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ตงคาToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.สรางชอแผนกและกลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.สรางกลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดตำแหนงToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดวนหยดประจำปToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดคาประกนสงคมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.จดกลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ยายสมาชกในกลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.แตงตงหวหนากลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 51)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(5, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1881, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MenuStrip2
        '
        Me.MenuStrip2.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.บนทกประวตToolStripMenuItem, Me.การนำเขาขอมลToolStripMenuItem, Me.บนทกขอมลตางๆToolStripMenuItem, Me.รายงานToolStripMenuItem, Me.ตงคาToolStripMenuItem, Me.กลมงานToolStripMenuItem})
        Me.MenuStrip2.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Padding = New System.Windows.Forms.Padding(5, 2, 0, 2)
        Me.MenuStrip2.Size = New System.Drawing.Size(1881, 51)
        Me.MenuStrip2.TabIndex = 1
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'บนทกประวตToolStripMenuItem
        '
        Me.บนทกประวตToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.รายวนToolStripMenuItem, Me.รายเดอนToolStripMenuItem, Me.กำหนดเงนเดอนToolStripMenuItem, Me.กำหนดเงนรายวนToolStripMenuItem})
        Me.บนทกประวตToolStripMenuItem.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.บนทกประวตToolStripMenuItem.Name = "บนทกประวตToolStripMenuItem"
        Me.บนทกประวตToolStripMenuItem.Size = New System.Drawing.Size(147, 47)
        Me.บนทกประวตToolStripMenuItem.Text = "บันทึกประวัติ"
        '
        'รายวนToolStripMenuItem
        '
        Me.รายวนToolStripMenuItem.Name = "รายวนToolStripMenuItem"
        Me.รายวนToolStripMenuItem.Size = New System.Drawing.Size(275, 48)
        Me.รายวนToolStripMenuItem.Text = "รายวัน"
        '
        'รายเดอนToolStripMenuItem
        '
        Me.รายเดอนToolStripMenuItem.Name = "รายเดอนToolStripMenuItem"
        Me.รายเดอนToolStripMenuItem.Size = New System.Drawing.Size(275, 48)
        Me.รายเดอนToolStripMenuItem.Text = "รายเดือน"
        '
        'กำหนดเงนเดอนToolStripMenuItem
        '
        Me.กำหนดเงนเดอนToolStripMenuItem.Name = "กำหนดเงนเดอนToolStripMenuItem"
        Me.กำหนดเงนเดอนToolStripMenuItem.Size = New System.Drawing.Size(275, 48)
        Me.กำหนดเงนเดอนToolStripMenuItem.Text = "กำหนดเงินเดือน"
        '
        'กำหนดเงนรายวนToolStripMenuItem
        '
        Me.กำหนดเงนรายวนToolStripMenuItem.Name = "กำหนดเงนรายวนToolStripMenuItem"
        Me.กำหนดเงนรายวนToolStripMenuItem.Size = New System.Drawing.Size(275, 48)
        Me.กำหนดเงนรายวนToolStripMenuItem.Text = "กำหนดเงินรายวัน"
        '
        'การนำเขาขอมลToolStripMenuItem
        '
        Me.การนำเขาขอมลToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.นำเขาขอมลจากFlashdriveToolStripMenuItem})
        Me.การนำเขาขอมลToolStripMenuItem.Name = "การนำเขาขอมลToolStripMenuItem"
        Me.การนำเขาขอมลToolStripMenuItem.Size = New System.Drawing.Size(171, 47)
        Me.การนำเขาขอมลToolStripMenuItem.Text = "การนำเข้าข้อมูล"
        '
        'นำเขาขอมลจากFlashdriveToolStripMenuItem
        '
        Me.นำเขาขอมลจากFlashdriveToolStripMenuItem.Name = "นำเขาขอมลจากFlashdriveToolStripMenuItem"
        Me.นำเขาขอมลจากFlashdriveToolStripMenuItem.Size = New System.Drawing.Size(371, 38)
        Me.นำเขาขอมลจากFlashdriveToolStripMenuItem.Text = "นำเข้าข้อมูลจาก flashdrive"
        '
        'บนทกขอมลตางๆToolStripMenuItem
        '
        Me.บนทกขอมลตางๆToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ขอทำลวงเวลารายบคคลToolStripMenuItem, Me.บนทกขอมลขอออกกอนToolStripMenuItem, Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem, Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem, Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem, Me.บนทกขอเขางานสายToolStripMenuItem, Me.บนทกการลาToolStripMenuItem, Me.บนทกคาเสยหายToolStripMenuItem, Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem})
        Me.บนทกขอมลตางๆToolStripMenuItem.Name = "บนทกขอมลตางๆToolStripMenuItem"
        Me.บนทกขอมลตางๆToolStripMenuItem.Size = New System.Drawing.Size(181, 47)
        Me.บนทกขอมลตางๆToolStripMenuItem.Text = "บันทึกข้อมูลต่างๆ"
        '
        'ขอทำลวงเวลารายบคคลToolStripMenuItem
        '
        Me.ขอทำลวงเวลารายบคคลToolStripMenuItem.Name = "ขอทำลวงเวลารายบคคลToolStripMenuItem"
        Me.ขอทำลวงเวลารายบคคลToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.ขอทำลวงเวลารายบคคลToolStripMenuItem.Text = "บันทึกขอทำล่วงเวลารายบุคคล"
        '
        'บนทกขอมลขอออกกอนToolStripMenuItem
        '
        Me.บนทกขอมลขอออกกอนToolStripMenuItem.Name = "บนทกขอมลขอออกกอนToolStripMenuItem"
        Me.บนทกขอมลขอออกกอนToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.บนทกขอมลขอออกกอนToolStripMenuItem.Text = "บันทึกข้อมูลขอออกก่อนรายบุคคล"
        '
        'บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem
        '
        Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem.Name = "บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem"
        Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem.Text = "บันทึกการทำงานล่วงเวลาแบบกลุ่ม"
        '
        'บนทกขอออกงานกอนแบบกลมToolStripMenuItem
        '
        Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem.Name = "บนทกขอออกงานกอนแบบกลมToolStripMenuItem"
        Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem.Text = "บันทึกขอออกงานก่อนแบบกลุ่ม"
        '
        'บนทกขอเขางานสายToolStripMenuItem
        '
        Me.บนทกขอเขางานสายToolStripMenuItem.Name = "บนทกขอเขางานสายToolStripMenuItem"
        Me.บนทกขอเขางานสายToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.บนทกขอเขางานสายToolStripMenuItem.Text = "บันทึกขอเข้างานสาย"
        '
        'บนทกการลาToolStripMenuItem
        '
        Me.บนทกการลาToolStripMenuItem.Name = "บนทกการลาToolStripMenuItem"
        Me.บนทกการลาToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.บนทกการลาToolStripMenuItem.Text = "บันทึกการลา"
        '
        'บนทกคาเสยหายToolStripMenuItem
        '
        Me.บนทกคาเสยหายToolStripMenuItem.Name = "บนทกคาเสยหายToolStripMenuItem"
        Me.บนทกคาเสยหายToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.บนทกคาเสยหายToolStripMenuItem.Text = "บันทึกค่าเสียหาย"
        '
        'บนทกเปลยนOTเปนสะสมToolStripMenuItem
        '
        Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem.Name = "บนทกเปลยนOTเปนสะสมToolStripMenuItem"
        Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem.Text = "บันทึกเปลี่ยนOTเป็นสะสม"
        '
        'รายงานToolStripMenuItem
        '
        Me.รายงานToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.รายงานการเขาหองนำToolStripMenuItem, Me.การคนหาขอมลตงๆToolStripMenuItem})
        Me.รายงานToolStripMenuItem.Name = "รายงานToolStripMenuItem"
        Me.รายงานToolStripMenuItem.Size = New System.Drawing.Size(95, 47)
        Me.รายงานToolStripMenuItem.Text = "รายงาน"
        '
        'รายงานการเขาหองนำToolStripMenuItem
        '
        Me.รายงานการเขาหองนำToolStripMenuItem.Name = "รายงานการเขาหองนำToolStripMenuItem"
        Me.รายงานการเขาหองนำToolStripMenuItem.Size = New System.Drawing.Size(310, 38)
        Me.รายงานการเขาหองนำToolStripMenuItem.Text = "รายงานการเข้าห้องน้ำ"
        '
        'การคนหาขอมลตงๆToolStripMenuItem
        '
        Me.การคนหาขอมลตงๆToolStripMenuItem.Name = "การคนหาขอมลตงๆToolStripMenuItem"
        Me.การคนหาขอมลตงๆToolStripMenuItem.Size = New System.Drawing.Size(310, 38)
        Me.การคนหาขอมลตงๆToolStripMenuItem.Text = "รายงานข้อมูลต่างๆ"
        '
        'ตงคาToolStripMenuItem
        '
        Me.ตงคาToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.สรางชอแผนกและกลมงานToolStripMenuItem, Me.สรางกลมงานToolStripMenuItem, Me.กำหนดตำแหนงToolStripMenuItem, Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem, Me.กำหนดวนหยดประจำปToolStripMenuItem, Me.กำหนดคาประกนสงคมToolStripMenuItem})
        Me.ตงคาToolStripMenuItem.Name = "ตงคาToolStripMenuItem"
        Me.ตงคาToolStripMenuItem.Size = New System.Drawing.Size(75, 47)
        Me.ตงคาToolStripMenuItem.Text = "ตั้งค่า"
        '
        'สรางชอแผนกและกลมงานToolStripMenuItem
        '
        Me.สรางชอแผนกและกลมงานToolStripMenuItem.Name = "สรางชอแผนกและกลมงานToolStripMenuItem"
        Me.สรางชอแผนกและกลมงานToolStripMenuItem.Size = New System.Drawing.Size(364, 38)
        Me.สรางชอแผนกและกลมงานToolStripMenuItem.Text = "สร้างแผนก"
        '
        'สรางกลมงานToolStripMenuItem
        '
        Me.สรางกลมงานToolStripMenuItem.Name = "สรางกลมงานToolStripMenuItem"
        Me.สรางกลมงานToolStripMenuItem.Size = New System.Drawing.Size(364, 38)
        Me.สรางกลมงานToolStripMenuItem.Text = "สร้างกลุ่มงาน"
        '
        'กำหนดตำแหนงToolStripMenuItem
        '
        Me.กำหนดตำแหนงToolStripMenuItem.Name = "กำหนดตำแหนงToolStripMenuItem"
        Me.กำหนดตำแหนงToolStripMenuItem.Size = New System.Drawing.Size(364, 38)
        Me.กำหนดตำแหนงToolStripMenuItem.Text = "กำหนดตำแหน่ง"
        '
        'กำหนดเวลาเขาออกงานกะToolStripMenuItem
        '
        Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem.Name = "กำหนดเวลาเขาออกงานกะToolStripMenuItem"
        Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem.Size = New System.Drawing.Size(364, 38)
        Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem.Text = "กำหนดเวลาเข้าออกงาน(กะ)"
        '
        'กำหนดวนหยดประจำปToolStripMenuItem
        '
        Me.กำหนดวนหยดประจำปToolStripMenuItem.Name = "กำหนดวนหยดประจำปToolStripMenuItem"
        Me.กำหนดวนหยดประจำปToolStripMenuItem.Size = New System.Drawing.Size(364, 38)
        Me.กำหนดวนหยดประจำปToolStripMenuItem.Text = "กำหนดวันหยุดประจำปี"
        '
        'กำหนดคาประกนสงคมToolStripMenuItem
        '
        Me.กำหนดคาประกนสงคมToolStripMenuItem.Name = "กำหนดคาประกนสงคมToolStripMenuItem"
        Me.กำหนดคาประกนสงคมToolStripMenuItem.Size = New System.Drawing.Size(364, 38)
        Me.กำหนดคาประกนสงคมToolStripMenuItem.Text = "กำหนดค่าประกันสังคม"
        '
        'กลมงานToolStripMenuItem
        '
        Me.กลมงานToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.จดกลมงานToolStripMenuItem, Me.ยายสมาชกในกลมงานToolStripMenuItem, Me.แตงตงหวหนากลมToolStripMenuItem})
        Me.กลมงานToolStripMenuItem.Name = "กลมงานToolStripMenuItem"
        Me.กลมงานToolStripMenuItem.Size = New System.Drawing.Size(101, 47)
        Me.กลมงานToolStripMenuItem.Text = "กลุ่มงาน"
        '
        'จดกลมงานToolStripMenuItem
        '
        Me.จดกลมงานToolStripMenuItem.Name = "จดกลมงานToolStripMenuItem"
        Me.จดกลมงานToolStripMenuItem.Size = New System.Drawing.Size(310, 38)
        Me.จดกลมงานToolStripMenuItem.Text = "จัดกลุ่มงาน"
        '
        'ยายสมาชกในกลมงานToolStripMenuItem
        '
        Me.ยายสมาชกในกลมงานToolStripMenuItem.Name = "ยายสมาชกในกลมงานToolStripMenuItem"
        Me.ยายสมาชกในกลมงานToolStripMenuItem.Size = New System.Drawing.Size(310, 38)
        Me.ยายสมาชกในกลมงานToolStripMenuItem.Text = "ย้ายสมาชิกในกลุ่มงาน"
        '
        'แตงตงหวหนากลมToolStripMenuItem
        '
        Me.แตงตงหวหนากลมToolStripMenuItem.Name = "แตงตงหวหนากลมToolStripMenuItem"
        Me.แตงตงหวหนากลมToolStripMenuItem.Size = New System.Drawing.Size(310, 38)
        Me.แตงตงหวหนากลมToolStripMenuItem.Text = "แต่งตังหัวหน้ากลุ่ม"
        '
        'บนทกขอทำชดเชยรายบคคลToolStripMenuItem
        '
        Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem.Name = "บนทกขอทำชดเชยรายบคคลToolStripMenuItem"
        Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem.Size = New System.Drawing.Size(421, 38)
        Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem.Text = "บันทึกขอทำชดเชยรายบุคคล"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1881, 1152)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.MenuStrip2)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "frmMain"
        Me.Text = "frmMain"
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents MenuStrip2 As MenuStrip
    Friend WithEvents บนทกประวตToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายวนToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายเดอนToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ตงคาToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents สรางชอแผนกและกลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดตำแหนงToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดเวลาเขาออกงานกะToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดวนหยดประจำปToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดคาประกนสงคมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents การนำเขาขอมลToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents นำเขาขอมลจากFlashdriveToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอมลตางๆToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ขอทำลวงเวลารายบคคลToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอมลขอออกกอนToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอออกงานกอนแบบกลมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกคาเสยหายToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายงานการเขาหองนำToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents การคนหาขอมลตงๆToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกการลาToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents สรางกลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกเปลยนOTเปนสะสมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดเงนเดอนToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดเงนรายวนToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอเขางานสายToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents จดกลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ยายสมาชกในกลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents แตงตงหวหนากลมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอทำชดเชยรายบคคลToolStripMenuItem As ToolStripMenuItem
End Class
