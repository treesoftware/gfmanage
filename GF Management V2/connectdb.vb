﻿Imports MySql.Data.MySqlClient
Module connectdb
    Dim objConn As MySqlConnection
    Dim objCmd As MySqlCommand
    Dim da As New MySqlDataAdapter

    Public Function querysql(ByVal table As String, ByVal sqlcom As String)
        Dim ds As New DataSet

        If objConn.State = False Then objConn.Open()
        da = New MySqlDataAdapter(sqlcom, objConn)
        da.Fill(ds, table)
        objConn.Close()
        da.Dispose()
        objConn.Dispose()

        Return ds
    End Function

    Public Sub connectsql()
        Dim strConnString As String
        Dim strDatabaseServerIp, strDatabaseName, strDatabaseUsername, strDatabasePassword As String
        strDatabaseServerIp = My.MySettings.Default.DatabaseServerIp
        strDatabaseName = My.MySettings.Default.DatabaseName
        strDatabaseUsername = My.MySettings.Default.DatabaseUsername
        strDatabasePassword = My.MySettings.Default.DatabasePassword

        strConnString = "Server=" & strDatabaseServerIp & ";Database=" & strDatabaseName & ";Uid=" & strDatabaseUsername & ";Pwd=" & strDatabasePassword & ";"
        objConn = New MySqlConnection(strConnString)

    End Sub
End Module
