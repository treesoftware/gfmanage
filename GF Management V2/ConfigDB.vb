﻿Public Class ConfigDB
    Private Sub btSave_Click(sender As Object, e As EventArgs) Handles btSave.Click
        Write()
        Read()
    End Sub

    Private Sub Read()
        txtServername.Text = My.MySettings.Default.DatabaseServerIp
        txtDatabasename.Text = My.MySettings.Default.DatabaseName
        txtUsername.Text = My.MySettings.Default.DatabaseUsername
        txtPassword.Text = My.MySettings.Default.DatabasePassword
    End Sub

    Private Sub Write()
        My.MySettings.Default.DatabaseServerIp = txtServername.Text
        My.MySettings.Default.DatabaseName = txtDatabasename.Text
        My.MySettings.Default.DatabaseUsername = txtUsername.Text
        My.MySettings.Default.DatabasePassword = txtPassword.Text
        MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub ConfigDB_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Read()
    End Sub

    Private Sub btCancel_Click(sender As Object, e As EventArgs) Handles btCancel.Click
        If MessageBox.Show("คุณต้องการที่จะยกเลิกการบันทึกข้อมูลใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            txtServername.Clear()
            txtDatabasename.Clear()
            txtUsername.Clear()
            txtPassword.Clear()
        End If

    End Sub
End Class